const Course = require("../models/Course.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");
const { get } = require("mongoose");
const { all, param } = require("../routes/userRoutes.js");


// Creates new course
module.exports.addCourse = (userData, courseData) => 
{
    if (userData.isAdmin) 
    {
        let newCourse = new Course({
            name: courseData.name,
            description: courseData.description,
            price: courseData.price
        });
        return newCourse.save().then((course, error) =>
        {
            if (error) 
            {
                console.log(error)
                return false;
            } 
            else 
            {
                return course;
            }
        })
    } 
    else 
    {
        return Promise.reject('Unauthorised user');
    }
};

// retrieve all courses

module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    })
};

// retrieve all active courses
module.exports.getAllActive = () => {
    return Course.find({isActive: true}).then(result => {
        return result;
    })
};

// retrieving a specifc course
/* module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result;
    })
}; */

module.exports.updateCourse = (reqParams,reqBody) => {
    let updateCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        isActive: reqBody.isActive
    }
    // findByIdAndUpdate - its purpose is to find a specific id in the database (first parameter) and update it using the information coming from the request body (second parameter)
    /* 
    findByIdAndUpdate (documentId, updatesToBeApplied)
    */
    return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course,err) => {
        if(err) {
        return false;
    } else 
    {
        return true;
    }
});
};

module.exports.archiveCourse = (reqParams,reqBody) => {
    let updateActiveField = {
        isActive: false
    }
    
    return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course,err) => {
        if(err) {
        return false;
    } else 
    {
        return true;
    }
});
};



