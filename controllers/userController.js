const User = require("../models/Users.js");

const auth = require("../auth.js");

const bcrypt = require("bcrypt");
const { get } = require("mongoose");
const { find, findOne } = require("../models/Users.js");
const Course = require("../models/Course.js");
/* 
1. use mongoose method "find" to find duplicate emails
2. use .then method to send a response based on the result of the find method
*/

module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
        // if there is an existing duplicate email
        if (result.length > 0){
           return true;
        } else {
            // if the user email is not yet registered in our database
            return false;
        }
    })
}

module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        // hashSync - bcrypt's method for encrypting the password of the user once they have successfully registered in our database
        /* 
        first parameter - the value to which the encryption will be done - password coming from the request body
        "10" - it dictates how many "salts" rounds are to be given to ecnrypt the value
        */
        password: bcrypt.hashSync(reqBody.password, 10)
    })
    return newUser.save().then((user,error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    })
};

// User Login
/* 
1. Check the database if the user email exists 
2. Compare the password provided in the request body with the password stored in the database
3. Generate/return a JSON web token if the user has successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result =>{
        // if user does not exist
		if(result === null){
			return false;
            // if the user email exists in the database
		} else {
            // compareSync will decode result.password, then compares the value with the value of reqBody.password
            // it's good practice that if the value returned by a method/function is boolean, the variable name should be answerable by yes/no
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordCorrect){
                return {access: auth.createAcessToken(result)}
            } else {
                return false;
            }
        }
    })
};

// Activity S38

module.exports.getDetails = (data) => {
	console.log(data);
	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};

// enroll a user to a class
/* 
1. to find the document in the database using the user's ID
2. add the course ID to the user's enrollments array
3. udpate the document in the MongoDB Atlas DB
*/
module.exports.enroll = async(data) => {
    let isUserUpdated = await User.findById(data.userId).then(user => {
        // adding the courseId in the enrollments aray of the user
        // returns boolean depending if the the updating of the document is successful (true) or failed (false)
        user.enrollments.push({courseId: data.courseId});
        // saves the updated user information in the database
        return user.save().then((user,error) => {
            if (error){
                return false;
            } else {
                return true;
            }
        })
    })
    let isCourseUpdated = await Course.findById(data.courseId).then(course => {
        // adding of userId in the enrollees array
        course.enrollees.push({userId: data.userId});
        // saves the updated course information in the database
        return course.save().then((course,error) => {
            if (error){
                return false;
            } else {
                return true;
            }
        })

    })

    // condition that will check if the user and course documents have been updated
    if (isUserUpdated && isCourseUpdated) {
        // User enrollment successfule
        return true;
    } else {
        // User enrollment failed
        return false;
    }
}

