const jwt = require("jsonwebtoken");
// used in algorithm for encrypting our data which makes it difficult to decode the information without the defined secret
const secret = "CrushAkoNgCrushKo";
/* 
JSONWEBTOKEN
-JSON WEB TOKEN or JWT is a way of securely passing information from the server to the frontend or to other parts of sercer
-information is kept secure through the use of the secret code
-Only the system that knows the secret code can decode the encrypted information 
*/
// https://jwt.io/
module.exports.createAcessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };

    // Generate a JSON web token using the jwt's sign method
    // Generates the token using the form data and the secret code with no additional options provided
    return jwt.sign(data, secret,{});
};


//Token Verification
module.exports.verify = (req,res,next) => {
    // the token is received from the request header
    // POSTMAN - Authorization - Bearer Token
    let token = req.headers.authorization

    if (typeof token !== "undefined"){
        console.log(token);
        // the token sent  is a type of "Bearer " token which when received, contains the "Bearer" as a prefix to the string
        token = token.slice(7,token.length)
        // verify() - validates the token decrypting the token using the secret code
        return jwt.verify(token,secret,(err,data) => {
            // if the jwt is invalid
            if (err){
                return res.send({auth:"failed"});
            } else {
                //  allows the application to proceed with the next middleware function/callback function in the route
                next();
            }
        })
    } else {
        return res.send({auth:"failed"});
    }
};

// Token decryption

module.exports.decode = (token) => {
    if (typeof token !== "undefined") {
        token = token.slice(7,token.length);
        return jwt.verify(token, secret, (err,data) => {
            if (err) {
                return null;
            } else {
                // decode method is used to obtain information from the JWT
                // the "token" argument serves as the one to be decoded
                // {complete:true} - option that allows us to return additional information from the JWT token
                // .payload property contains the information provided in the "createAccessToken" method defined above (values for id, email, )
                return jwt.decode(token, {complete: true}).payload;
            }
        })
    } else{
        // if the token does not exist
        return null;
    
    };
};