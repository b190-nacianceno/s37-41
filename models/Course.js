const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
    name: {
        type: String,
        // the server requires the data for this field to be completed when creating a document/record
        required: [true,"Course name is required"]
    },
    description:{
        type: String,
        required:[true,"Price is required"]
    },
    isActive:{
        type: Boolean,
        // when null returns true
        default: true 
    },
    createdOn:{
        type: Date,
        // when null, instantiates the current date when the record was created
        default: new Date()
    },
    enrollees: [
        {
            userId: {
                type: String,
                required: [true, "userId is required"]
            },
            enrolledOn: {
                type: Date,
               default: new Date()
            }
        }
    ]
});

module.exports = mongoose.model("Course",courseSchema);